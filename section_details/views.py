# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render,redirect
from .models import sec
from .forms import sectionform 


# Create your views here.



def insert_view(request):
    form = sectionform()
    if request.method == 'POST':
        form = sectionform(request.POST)
        if form.is_valid():
           form.save()
           return redirect('/')    
    return render(request,'homepage.html',{'form':form})

# def show_view(request):
#      return render(request,'display.html')

def student_details(request):
    sections = sec.objects.all()
    print(sections)
    return render(request,"display.html",{'sections':sections})

