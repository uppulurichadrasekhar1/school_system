# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class sec(models.Model):
    grade=models.CharField(max_length=200)
    section=models.CharField(max_length=200)
    subject=models.CharField(max_length=200) 
     
    def _str_(self):
        return self.grade+' '+ self.section+' '+self.subject