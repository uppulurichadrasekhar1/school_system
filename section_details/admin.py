# -*- coding: utf-8 -*-
# from __future__ import unicode_literals
from django.contrib import admin
from .models import sec

# Register your models here.
class sectionAdmin(admin.ModelAdmin):
    list_display=['grade','section','subject']

admin.site.register(sec,sectionAdmin)